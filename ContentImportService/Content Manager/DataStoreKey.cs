﻿namespace ExportPackages_WS.Content_Manager
{
    public class DataStoreKey
    {
        #region Variables

        public string StoreName { get; private set; }
        public string UserName { get; private set; }
        public string UserPass { get; private set; }

        #endregion

        // ---------------- ---------------- ---------------- ---------------- ---------------- 

        public DataStoreKey(string StoreName, string UserName, string UserPass)
        {
            this.StoreName = StoreName;
            this.UserName = UserName;
            this.UserPass = UserPass;
        }

    }
}
