﻿using NLog;
using NLog.Config;

namespace ContentImportService.NLog
{
    public static class NLogWrap
    {
        public static void LoadConfig(string path)
        {
            LogManager.Configuration = new XmlLoggingConfiguration(path);
        }
    }
}