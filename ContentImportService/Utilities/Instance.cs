﻿using ContentImportService.Extensions;
using ContentImportService.NLog;
using NLog;

namespace ContentImportService.Utilities
{
    public static class Instance
    {
        public static string Location { get; private set; }

        public static ILogger Logger { get; private set; }

        public static ILogger Console { get; private set; }

        static Instance()
        {
            Location = Assemblies.ExecutingLocation.Folder();

            NLogWrap.LoadConfig(Location.File("NLog/Config.xml"));

            Logger = LogManager.GetLogger("Default");
            Console = LogManager.GetLogger("Console");
        }

        public static void PressAnyKeyToExit()
        {
            Console.Info("Press any key to exit");
            System.Console.ReadKey();
        }
    }
}