﻿using System;
using System.Collections.Generic;
using System.IO;

using ContentImportService.Utilities;
using Quartz;
using Quartz.Impl;
using Topshelf;

namespace ContentImportService.ServiceWrap
{
    public class Service
    {
		public static void Run(string serviceName, string displayName, string description, params Workflow[] workflows)
        {
            HostFactory.Run(configurator =>
            {
                configurator.Service<Service>(settings =>
                {
                    settings.ConstructUsing(service => new Service(workflows));

                    settings.WhenStarted(service => service.Start());
                    settings.WhenStopped(service => service.Stop());
                });

                configurator.RunAsLocalSystem();

                configurator.SetServiceName(serviceName);
                configurator.SetDisplayName(displayName);
                configurator.SetDescription(description);
            });
        }
		
		// ------------------ ------------------ ------------------ ------------------ ------------------ 
		
        private List<Guid> Workflows { get; set; }

        // ------------------ ------------------ ------------------ ------------------ ------------------ 

        private IScheduler Scheduler { get; set; }

        private Dictionary<Guid, IJobDetail> JobDetails { get; set; }

        private Dictionary<Guid, ITrigger> Triggers { get; set; }

        // ------------------ ------------------ ------------------ ------------------ ------------------ 

        public Service(params Workflow[] workflows)
        {
			Directory.SetCurrentDirectory(Instance.Location);
			
            Workflows = new List<Guid>();

            JobDetails = new Dictionary<Guid, IJobDetail>();
            Triggers = new Dictionary<Guid, ITrigger>();

            foreach (var workflow in workflows)
            {
                var workflowId = Guid.NewGuid();
                Workflows.Add(workflowId);

                PrepareScheduler(workflowId, 
                    workflow.WorkflowType, 
                    workflow.RecycleInSec);
            }
        }

        public void PrepareScheduler(Guid workflowId, Type workflowType, int restartSecs)
        {
            Scheduler = StdSchedulerFactory.GetDefaultScheduler();

            var jobDetail = JobBuilder.Create()
                .WithIdentity("Workflow: " + workflowId, "Service")
                .OfType(workflowType)
                .Build();

            JobDetails.Add(workflowId, jobDetail);

            var trigger = TriggerBuilder.Create()
                .WithIdentity("Trigger: " + workflowId, "Service")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(restartSecs)
                    .RepeatForever())
                .Build();

            Triggers.Add(workflowId, trigger);
        }

        // ------------------ ------------------ ------------------ ------------------ ------------------ 

        public void Start()
        {
            Instance.Logger.Info("Запуск Workflow");

            Scheduler.Start();

            foreach (var jobDetail in JobDetails)
                Scheduler.ScheduleJob(jobDetail.Value, Triggers[jobDetail.Key]);
        }

        public void Stop()
        {
            Instance.Logger.Info("Остановка Workflow");

            foreach (var jobDetail in JobDetails)
                Scheduler.Interrupt(jobDetail.Value.Key);

            Scheduler.Shutdown(true);
        }
    }
}
