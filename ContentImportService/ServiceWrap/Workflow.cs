﻿using System;

namespace ContentImportService.ServiceWrap
{
    public class Workflow
    {
        public Type WorkflowType { get; set; }

        public int RecycleInSec { get; set; }
    }
}
