﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SoDUploader;

namespace ContentImportService.Entities
{
    public class SetOfDocuments
    {
        public List<Document> Documents { get; set; }

        public string Barcode { get; set; }

        public string IsInOut { get; set; }

        public string CreatorLogin { get; set; }

        public string ContractorName { get; set; }

        public string ContractorCode { get; set; }

        public string ContractorITSN { get; set; }

        public int PagesCount { get; set; }

        public Employee Curator { get; set; }
    }

}