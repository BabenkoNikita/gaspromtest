﻿using System;

namespace SoDUploader
{
    public class Document
    {
        public string ItemId { get; set; }

        public string IsuDocType { get; set; }

        public string IsDocNum { get; set; }

        public string IsOrigDraftCopy { get; set; }

        public DateTime IsDocDate { get; set; }

        public string IsSumm { get; set; }

        public string IsDocNumOsn { get; set; }

        public string IsDocDateIs { get; set; }
    }
}