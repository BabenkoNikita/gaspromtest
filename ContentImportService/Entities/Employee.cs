﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentImportService.Entities
{
    public class Employee
    {
        public virtual int Code { get; set; }
        public virtual bool IsBlocked { get; set; }
        public virtual string Surname { get; set; }
        public virtual string Name { get; set; }
        public virtual string Fathername { get; set; }
        public virtual string Postion { get; set; }
        public virtual int ChildOrgCode { get; set; }
        public virtual int OrgUnitCode { get; set; }
        public virtual DateTime PendingTS { get; set; }
        public virtual string FullName { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual List<string> OrgStructure { get; set; }
    }
}
