﻿using System.Reflection;

namespace ContentImportService.Extensions
{
    public static class Assemblies
    {
        public static Assembly Entry
        {
            get { return Assembly.GetEntryAssembly(); }
        }

        public static string EntryLocation
        {
            get { return Entry.Location; }
        }

        public static Assembly Executing
        {
            get { return Assembly.GetExecutingAssembly(); }
        }

        public static string ExecutingLocation
        {
            get { return Executing.Location; }
        }

        public static Assembly Calling
        {
            get { return Assembly.GetCallingAssembly(); }
        }

        public static string CallingLocation
        {
            get { return Calling.Location; }
        }
    }
}