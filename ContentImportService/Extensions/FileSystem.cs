﻿using System.IO;

namespace ContentImportService.Extensions
{
    public static class FileSystem
    {
        public static string Folder(this string path)
        {
            return Path.GetDirectoryName(path);
        }

        public static string File(this string path, string subpath)
        {
            return Path.Combine(path, subpath);
        }

        public static string FileName(this string path, bool withExt = false)
        {
            return withExt ? Path.GetFileName(path) : Path.GetFileNameWithoutExtension(path);
        }
    }
}