﻿using System.IO;
using iTextSharp.text.pdf;

namespace ContentImportService.Extensions
{
    public static class PdfWrap
    {
        public static string MergeDocs(string mainDoc, string applDoc, string tempDoc, 
            bool deleteMain = true, bool deleteAppl = true)
        {
            using (var stream = new FileStream(tempDoc, FileMode.Create))
            {
                using (var document = new iTextSharp.text.Document())
                {
                    using (var copier = new PdfCopy(document, stream))
                    {
                        using (var readerFirst = new PdfReader(mainDoc))
                        {
                            document.SetPageSize(readerFirst.GetPageSize(1));
                            document.Open();
                            copier.AddDocument(readerFirst);
                            stream.Flush();
                        }

                        using (var readerOther = new PdfReader(applDoc))
                        {
                            copier.AddDocument(readerOther);
                            stream.Flush();
                        }

                        if(deleteAppl) File.Delete(applDoc);
                    }

                    stream.Close();
                    document.Close();
                }
            }

            if(deleteMain) File.Delete(mainDoc);

            return tempDoc;
        }
    }
}