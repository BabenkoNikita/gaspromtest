﻿using ContentImportService.Properties;
using ContentImportService.ServiceWrap;
using ContentImportService.Workflows;
using ImageMagick;

namespace ContentImportService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MagickNET.SetGhostscriptDirectory(Settings.Default.GhostScript);

            Service.Run("ContentImportService", "Content Import Service", string.Empty,
                new Workflow {WorkflowType = typeof (SampleWorkflow), RecycleInSec = 1});
        }
    }
}
