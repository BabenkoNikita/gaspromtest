﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ContentImportService.Content_Manager;
using ContentImportService.Entities;
using ContentImportService.Extensions;
using ContentImportService.Utilities;
using ExportPackages_WS.Content_Manager;
using ImageMagick;
using Newtonsoft.Json;
using Quartz;

namespace ContentImportService.Workflows
{
    [DisallowConcurrentExecution, PersistJobDataAfterExecution]
    public class SampleWorkflow : IInterruptableJob
    {
        const int CONTENT_FILE_LENGTH = 51;

        public bool Interrupted { get; private set; }

        public void Interrupt()
        {
            Instance.Logger.Debug($"Workflow has been interrupted");
            Interrupted = true;
        }

        public void Execute(IJobExecutionContext context)
        {
            Instance.Logger.Debug("Start of Workflow cycle");

            try
            {
                if (Interrupted)
                {
                    Instance.Logger.Info("Workflow interrupted");
                    return;
                }

                var sodPluginPath = Environment.ExpandEnvironmentVariables(
                    "%AppData%\\Incoming");

                var jsons = GetInputJsons(sodPluginPath);

                jsons.ToList().ForEach(j => HandleJson(j, sodPluginPath));
            }
            catch (Exception exception)
            {
                Instance.Logger.Error(exception);
            }
            finally
            {
                Instance.Logger.Debug("End of Workflow cycle");
            }
        }

        private static string MergeInSinglePdf(string sodPluginPath, ref Dictionary<int, string> filesDict)
        {
            var singlePDF = Path.Combine(sodPluginPath, Guid.NewGuid().ToString());

            File.Move(filesDict.First(it => it.Key == -1).Value, singlePDF);

            foreach (var file in filesDict.OrderBy(it => it.Key).Skip(1))
            {
                var tempPDF = Path.Combine(sodPluginPath, Guid.NewGuid() + ".pdf");
                singlePDF = PdfWrap.MergeDocs(singlePDF, file.Value, tempPDF);
            }

            return singlePDF;
        }

        private static void ConvertTiffToPdf(ref Dictionary<int, string> filesDict)
        {
            var changedList = new List<int>();
            foreach (var tiff in filesDict.Where(it => it.Value.FileName(true).EndsWith(".tiff")))
            {
                Instance.Logger.Info("Файл {0}", tiff.Value.FileName());

                using (var imageMagick = new MagickImage(new FileInfo(tiff.Value)))
                {
                    var fileName = tiff.Value.FileName(true);
                    var newFileName = fileName.Replace(".tiff", ".pdf");

                    imageMagick.Format = MagickFormat.Pdf;
                    imageMagick.Write(new FileInfo(tiff.Value.Replace(fileName, newFileName)));
                }

                File.Delete(tiff.Value);
                changedList.Add(tiff.Key);
            }

            foreach (var changedFile in changedList)
            {
                filesDict[changedFile] = filesDict[changedFile].Replace(".tiff", ".pdf");
            }
        }

        private static IEnumerable<string> GetInputJsons(string sodPluginPath)
        {
            Instance.Logger.Trace("sodPluginPath = {0}", sodPluginPath);

            return Directory.GetFiles(sodPluginPath, "SoD_*.json");
        }

        private static IEnumerable<string> GetContentFiles(string sodPluginPath, string json)
        {
            Instance.Logger.Trace("sodPluginPath = {0}", sodPluginPath);

            return Directory.GetFiles(sodPluginPath, string.Format("Session_{0}_*",
                json.FileName().Replace("SoD_", "")));
        }

        private static Dictionary<int, string> GetDictionaryOfFiles(string sodPluginPath, string json)
        {
            var files = GetContentFiles(sodPluginPath, json);
            return files.ToDictionary(file =>
                int.Parse(file.FileName().Substring(CONTENT_FILE_LENGTH)));
        }

        private static string CombineSinglePdf(ref Dictionary<int, string> filesDict, string sodPluginPath)
        {
            ConvertTiffToPdf(ref filesDict);

            var firstFile = filesDict[-1];
            var singlePDF = MergeInSinglePdf(sodPluginPath, ref filesDict);

            File.Move(singlePDF, firstFile);
            singlePDF = firstFile;

            return singlePDF;
        }

        private static void CreateDocumentInStore(SetOfDocuments sodInfo, string singlePDF)
        {
            using (var store = new DataStore(new DataStoreKey("mainStore", "guest", "qwerty12345")))//TODO: in separate method
            {
                var mainId = store.CreateDocument(store, new Dictionary<string, string>
                        {
                            {"Entity", "Main"},
                            {"DM_Class", "основной"},
                            {"DM_State", "Верен"},
                            {"isOrigDraftCopy", sodInfo.Documents.First().IsOrigDraftCopy},
                            {"docCreator", sodInfo.CreatorLogin},
                            {"Barcode", sodInfo.Barcode},
                            {"isInOut", sodInfo.IsInOut},
                            {"pageCount", sodInfo.PagesCount + ""},
                            {"Mimetype", "application/pdf"},
                            {
                                "Content", singlePDF
                            },

                            {"NameOfContractor", sodInfo.ContractorName},
                            {"CodeOfContractor", sodInfo.ContractorCode},
                            {"ITSNOfContractor", sodInfo.ContractorITSN},

                            {"IsuDocType", sodInfo.Documents.First().IsuDocType},
                            {"IsDocNum", sodInfo.Documents.First().IsDocNum},
                            {"IsOrigDraftCopy", sodInfo.Documents.First().IsOrigDraftCopy},
                            // {"IsuDocType", sodInfo.Documents.First().IsDocDate},
                            {"IsSumm", sodInfo.Documents.First().IsSumm}
                        });

                File.Delete(singlePDF);
            }
        }

        private static void HandleJson(string json, string sodPluginPath)
        {
            Instance.Logger.Trace("json: {0}", json.FileName());

            var sodInfo = (SetOfDocuments)JsonConvert.DeserializeObject(
                File.ReadAllText(json), typeof(SetOfDocuments));

            var filesDict = GetDictionaryOfFiles(sodPluginPath, json);

            var singlePDF = CombineSinglePdf(ref filesDict, sodPluginPath);

            Instance.Logger.Info("Загрузка в CM");

            CreateDocumentInStore(sodInfo, singlePDF);

            File.Delete(json);
        }

    }
}
